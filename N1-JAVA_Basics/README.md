![TBZ Logo](../x_gitressourcen/tbz_logo.png)
![m319 Picto](../x_gitressourcen/m319_picto.jpg)

[TOC]

# JAVA Basics

[English version](#english-version)

---
### Lernziel:
* Wie wird ein Programm erstellt.
* Grundbegriffe von JAVA 
* Aufbau eines Programms
* Erste Schritte mit Java (Template)

--- 

## Wie lerne ich zu Programmieren?

... bzw. wie lerne ich eine Programmiersprache?

**Antwort** <br>
Wie bei einer Fremdsprache …

![Programmieren](./x_gitressourcen/ProgrLernen.jpg)


## Vom Programmtext zur Ausführung

Ein Quellcode wird grundsätzlich entweder in "kompiliert" oder "interpretiert":

![Comp_Inter](./x_gitressourcen/Comp_Inter.png)

**Entwicklungszeit** Das Programm ist in der Entwicklung (Editor) <br>
**Laufzeit** Das Programm wird ausgeführt <br>
**Compiler** sind Programme (sog. "Host"), die den mit einem Editor geschriebenen Programmtext in Maschienensprache *übersetzen*. <br> Vorteil: Schnelle Ausführung des Programms <br>
**Interpreter** sind Programme (sog. "Host"), die den mit einem Editor geschriebenen Programmtext  *direkt ausführen*. <br> Vorteil: Flexible Ausführung, oft plattformunabhängig



Moderne Programmiersprachen benutzen beide Vorteile:
![Java Programmieren](./x_gitressourcen/JavaCompInter.jpg)
Der **JAVA-Compiler** ist ein Programm (javac.exe), das .java-Quelldateien in einen *universellen, plattformunabhängigen Byte-Code* übersetzt. (.class / .jar) <br>
Der **JAVA-Interpreter** führt den universellen Byte-Code) direkt in der *plattform-abhängigen virtuellen Maschine* aus. (java.exe in JVM)

>[>>> Wollen Sie mehr wissen? (Guru) <<<](https://www.guru99.com/java-virtual-machine-jvm.html)

![ToDo](../x_gitressourcen/ToDo.png) Do To:

Nehmen Sie eine .java-Datei und bringen Sie es mit den CL-Tools der JDK in der Konsole zum Laufen ...

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Seite 15](../Knowledge_Library/Java_Programmieren.pdf)

---

## JAVA Grundelemente:

[![Buch](../x_gitressourcen/Buch.jpg)Unterlagen Kap.1.1 bis 1.5.2](../Knowledge_Library/Java_Programmieren.pdf)

[Siehe auch "TOPIC Variables & Constants"](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N1-Variables_Constants)

Die Struktur eines (einfachen) Programms ist vorgegeben:

[Programm Aufbau (W3S)](https://www.w3schools.com/java/java_syntax.asp)

![Struktur einfach](./x_gitressourcen/Struktur_einfach.jpg)

**Projekt**: In einer IDE wir jedes Programm als Projekt verwaltet. Um ein neues Programm zu schreiben müssen sie darum ein Projekt eröffnen. <br>

**Package**: Oft ist ein Programm in ein Paket geschnürt, welches dem umgekehrten Domain Name der Firma entspricht. Die Paketbezeichnung wird als erste Zeile ins .java-File geschrieben. Über Pakete können Sie ein Projekt strukturieren. <br>

**import**: Müssen vor dem Klassennamen angegeben. <br>

**class static _name_**: Jedes Programm muss in einer Klasse eingebunden sein. Die Klasse muss wie die Java-Datei heissen und vice versa..<br>

**public static void main ()**: Hier beginnt das Hauptprogramm ...  <br>

**code**: ...und der Code darin wird ausgeführt.


### Hier ein einfaches Beispiel: (GallonsConverter.java)

```java
public class GallonsConverter {

   // our main function which runs the program
	public static void main( String[] args ) {
		
		//defining variables:
		double gallons;
		double litres;
		
		//assigns a value:
		gallons = 10;   
		
		//calculates result:             
		litres = gallons / 3.7854;
		
		// print out using the System library:
		System.out.println(gallons + " gallons is " + litres + " litres.");
	
	}
}
```
Beachten Sie, dass der Kommentar (//...) nicht ausgeführt wird.

---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

## TBZ Projekt Template (IntelliJ)

1. Laden Sie die Datei ["Template.zip"](./Template.zip) herunter 
2. und **entzippen** Sie es in ihrem m319-Arbeitsverzeichnis.
3. **Öffnen** (> **Open**) Sie den **Ordner "Template"** in der IDE (Achtung: Also keine Datei öffnen!)
4. und studieren Sie den Code der **main-Funktion**. 

![IDE Projekt](./x_gitressourcen/IDE Projekt.png)

Hier die Struktur von "main" und "Input": 

![Struktur Template](./x_gitressourcen/Struktur_template.jpg)

Die Bibliotheks-Klasse **Input** (in ```package ch.tbz.lib```) stellt **Funktionen zur Eingabe von Werten** bereit: <br> 
```=inputInt("Msg:")```, <br> 
```=inputFloat(...)```, <br> 
```=inputString()```, <br>
`=input...()` <br> 

![ToDo](../x_gitressourcen/ToDo.png) Do To:
Studieren Sie die *public Funktionen* der Bibliotheks-Klasse **Input**!
<br>

**Verwenden Sie dieses Template für weitere Aufgaben...**

>**Anm**.: Sie können beliebig viele, weitere Klassen (**Kontextmenü auf Eintrag ">ch.tbz" > New >Java Class **) mit einer "main"-Methode im Projekt einfügen; verwenden Sie einfach jeweils einen anderen Namen für die Klasse ... <br>
> Starten mit "**Menü >Run >Run...**"


---

![ToDo](../x_gitressourcen/ToDo.png) Do To:

## Erste Übungen für den Einstieg

### Einfache Berechnung

Schreiben Sie eine Klasse *Berechnung* wo wir die Addition / Subtraktion / Multiplikation / Division von zwei ganzen Zahlen berechnen können. Verwenden Sie dafür zwei entsprechende Variablen und weisen Sie den Variablen Werte zu. Die Berechnung und der Ausdruck erfolgt in der main-Funktion.

### Name und Adresse einlesen und ausdrucken

Schreiben Sie eine Klasse, wo Name und Adresse ausgegeben wird. Fordern Sie den User auf, seinen Namen und die Adresse einzugeben. Wir verwenden wiederum zwei Variablen.


## Weitere Links:
[Ausdrücke, Operanden und Operatoren](https://openbook.rheinwerk-verlag.de/javainsel/02_004.html#u2.4) <br>
[W3S Mathe Bibliothek](https://www.w3schools.com/java/java_ref_math.asp)


---

# Checkpoint
* Sie kennen den Weg vom Quelltext zum Output in der JAVA-Welt.
* Sie kennen die (drei) grundsätzlichen Konzepte der Programmerstellung (Übersetzung).
* Was ist ein "Ausdruck", ein "Literal", ein "Operator" und ein "Bezeichner".
* Kenne den strukturellen Aufbau eines JAVA Programms.
* Ein Projekt in der IDE mit "TBZ Template" eröffnet und ausgeführt.
* Kann ein einfaches Programm nachvollziehen.
* Kann mit eingegebenen Zahlen einfache Berechnungen kalkulieren.


<br><br><br>

# English version

## Learning goals:
* How do I create a program
* Java Basics
* Structuring a program
* First steps in Java (with a template)

## How do I program?

... or how do I learn a programming language?

**Answer** <br>
well, it's a bit like learning a foreign language:

![Programmieren](./x_gitressourcen/ProgrLernen.jpg)

## From a piece of code to execution

Code is either compiled or interpreted:

![Comp_Inter](./x_gitressourcen/Comp_Inter.jpg)

**Development** The program is being developed (in an Editor) <br>
**Runtime** The program is executed <br>
**Compiler** are programs ("Host"), which *translate* the code into machine language.<br> Advantage: quick execution of the program <br>
**Interpreter** are programs ("Host"), which are directly run<br> Advantage: flexible execution, platform independent 


Modern programming languages use both advantages:
![Java Programmieren](./x_gitressourcen/JavaCompInter.jpg)
The **JAVA-Compiler** is a program (javac.exe), which translates .java-source code into *universal, platform independent Byte-Code*. (.class / .jar) <br>
The **JAVA-Interpreter** runs universal Byte-Code) directly in the platform independent *virtual machine* aus.(java.exe in JVM)

## JAVA Basics:

[![Buch](../x_gitressourcen/Buch.jpg)See chap 1.1 to 1.5.2](../Knowledge_Library/Java_Programmieren.pdf)

[Also check "TOPIC Variables & Constants"](https://gitlab.com/ch-tbz-it/Stud/m319/-/tree/main/N1-Variables_Constants)

The structure of a basic program is always given:

[Syntax of a program (W3S)](https://www.w3schools.com/java/java_syntax.asp)

![Struktur einfach](./x_gitressourcen/Struktur_einfach.jpg)

**Project**: Each program is managed as a project in the IDE. <br>

**Package**: You can use packages to organize and structure your class files. The package name is written into the code at the beginning (first line).  <br>

**import**: Must be included before the class name. <br>

**class static _name_**: Each program must be included in a class. Class name and file name must be identical.<br>

**public static void main ()**: Begin of the main program to execute the code ...  <br>

**code**: ...code is executed.



### A simple program: (GallonsConverter.java)

```java
public class GallonsConverter {

   // our main function which runs the program
	public static void main( String[] args ) {
		
		//defining variables:
		double gallons;
		double litres;
		
		//assigns a value:
		gallons = 10;   
		
		//calculates result:             
		litres = gallons / 3.7854;
		
		// print out using the System library:
		System.out.println(gallons + " gallons is " + litres + " litres.");
	
	}
}
```
Comments are included with (//...) at the beginning of the line and are not executed.

## TBZ Project Template (IntelliJ)
Download the ["Template.zip"](./Template.zip) an unzip it in your M319 folder.
Open (> **Open**) the folder "Template" in your IDE (IntelliJ) and read through the code **main-Funktion**. 

![IDE Projekt](./x_gitressourcen/IDE Projekt.png)

Here's the structure of "main" and "Input": 

![Struktur Template](./x_gitressourcen/Struktur_template.jpg)

The library class **Input** (in ```package ch.tbz.lib```) provides **functions to read user input** : <br> 
```=inputInt("Msg:")```, <br> 
```=inputFloat(...)```, <br> 
```=inputString()```, <br>
`=input...()` <br> 

![ToDo](../x_gitressourcen/ToDo.png) Do To:
Study all the *public functions* of the library class **Input**!
<br>
**Use this template for other tasks...**

>**By the way**.: You can add more classes into your project by creating a new class (">ch.tbz" > New >Java Class ) , use a new name for a new class.<br>
> Run a program with "Menu > Run > Run"

---
## First Programs

### Simple Calculation
Write a class *Calculation* where we calculate an addition of two numbers. Use an integer as datatype and create two variables. The calculation is done in the main-method, and the print-out is done in the main-method.

### Show name & address
Write a class that prints out your name and address. Again, use two variables – one for your name. One for your address.

---

# Checkpoint
* You know the process from code to runnable output in Java.
* You know the three basic concepts of creating code (translation).
* You know an "expression", "literal", "operator" and "identifier"
* You know the structure of a java program.
* You have opened the TBZ project and run the code.
* You understand a simple program.
* You can do simple calculations with user input.
